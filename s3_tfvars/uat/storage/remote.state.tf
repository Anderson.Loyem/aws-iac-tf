terraform {
  backend "s3" {
    bucket = "terraform-aws-assurly"
    key    = "storage.uat.tfstate"
    region = "eu-west-3"
  }
}