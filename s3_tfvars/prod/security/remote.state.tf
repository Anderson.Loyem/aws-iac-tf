terraform {
  backend "s3" {
    bucket = "terraform-aws-assurly"
    key    = "security.prod.tfstate"
    region = "eu-west-3"
  }
}