terraform {
  backend "s3" {
    bucket = "terraform-aws-assurly"
    key    = "service.prod.tfstate"
    region = "eu-west-3"
  }
}