
/* add trigger to lambda */

resource "aws_api_gateway_method" "method" {
  rest_api_id      = var.aws_api_gateway_rest_api_id
  resource_id      = var.aws_api_gateway_resource_id
  http_method      = var.integration_http_method
  authorization    = "NONE"
  api_key_required = true
}


resource "aws_api_gateway_integration" "integration" {
  rest_api_id             = var.aws_api_gateway_rest_api_id
  resource_id             = var.aws_api_gateway_resource_id
  http_method             = aws_api_gateway_method.method.http_method
  integration_http_method = var.integration_http_method
  type                    = "AWS"
  uri                     = var.lambda_invoke_arn
  request_templates = {
    "application/json" = <<EOF
{
  "body" : $input.json('$'),
  "headers": {
    #foreach($param in $input.params().header.keySet())
    "$param": "$util.escapeJavaScript($input.params().header.get($param))" #if($foreach.hasNext),#end

    #end  
  },
  "stage" : "$context.stage"
}
EOF
  }
}

resource "aws_lambda_permission" "apigw_lambda" {
  statement_id  = "AllowExecutionFromAPIGateway"
  action        = "lambda:InvokeFunction"
  function_name = var.lambda_name
  principal     = "apigateway.amazonaws.com"
}

resource "aws_api_gateway_method_response" "response_200" {
  rest_api_id = var.aws_api_gateway_rest_api_id
  resource_id = var.aws_api_gateway_resource_id
  http_method = var.integration_http_method
  status_code = "200"
}

resource "aws_api_gateway_integration_response" "integration_esponse" {
  rest_api_id = var.aws_api_gateway_rest_api_id
  resource_id = var.aws_api_gateway_resource_id
  http_method = var.integration_http_method
  status_code = aws_api_gateway_method_response.response_200.status_code
  response_templates = {
    "application/json" = ""
  }
}
