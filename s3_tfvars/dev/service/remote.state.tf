terraform {
  backend "s3" {
    bucket = "terraform-aws-assurly"
    key    = "service.dev.tfstate"
    region = "eu-west-3"
  }
}