variable "env" {
  description = "env: dev or prod"
  default     = "dev"
}

variable "aws_region" {
  type        = string
  default     = "us-east-1"
  description = "region dev"
}

variable "aws_common_tag" {
  type        = map(any)
  description = "aws tag commun"
  default = {
    Organization = "assurly"
    App          = "assurly-back"
    Entity       = "service"
    env          = "dev"
  }
}
