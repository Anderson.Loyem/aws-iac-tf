
// https://registry.terraform.io/modules/lgallard/cognito-user-pool/aws/latest/examples/complete

module "aws_cognito_user_pool_complete" {

  source = "lgallard/cognito-user-pool/aws"

  user_pool_name           = "mypool_tf"
  #alias_attributes         = ["email", "phone_number"]
  #auto_verified_attributes = ["email"]

  admin_create_user_config = {
    email_subject = "Here, your verification code baby"
  }

  email_configuration = {
    email_sending_account  = "DEVELOPER"
    reply_to_email_address = "anderson.loyem@assurly.com"
    source_arn             = "arn:aws:ses:us-east-1:198985668425:identity/anderson.loyem@assurly.com"
  }

  password_policy = {
    minimum_length    = 8
    require_lowercase = false
    require_numbers   = true
    require_symbols   = true
    require_uppercase = true
    temporary_password_validity_days = 5
  }

  schemas = [
    {
      attribute_data_type      = "String"
      developer_only_attribute = false
      mutable                  = true
      name                     = "serverId"
      required                 = false
    }
  ]

  /*string_schemas = [
    {
      attribute_data_type      = "String"
      developer_only_attribute = false
      mutable                  = false
      name                     = "email"
      required                 = true

      string_attribute_constraints = {
        min_length = 7
        max_length = 15
      }
    }
  ]
  */
  recovery_mechanisms = [
    {
      name     = "verified_email"
      priority = 1
    },
    {
      name     = "verified_phone_number"
      priority = 2
    }
  ]

  # clients
  clients = [
    {
      allowed_oauth_flows                  = []
      allowed_oauth_flows_user_pool_client = false
      allowed_oauth_scopes                 = []
      #callback_urls                        = ["https://mydomain.com/callback"]
      #default_redirect_uri                 = "https://mydomain.com/callback"
      explicit_auth_flows                  = []
      generate_secret                      = true
      logout_urls                          = []
      name                                 = "ass"
      read_attributes                      = ["email", "custom:serverId"]
      write_attributes                      = ["email", "custom:serverId"]
      supported_identity_providers         = []
      write_attributes                     = []
      access_token_validity                = 1
      id_token_validity                    = 1
      refresh_token_validity               = 60
      token_validity_units = {
        access_token  = "hours"
        id_token      = "hours"
        refresh_token = "days"
      }
    }
  ]

lambda_config = {
    #create_auth_challenge          = "arn:aws:lambda:us-east-1:123456789012:function:create_auth_challenge"
    custom_message                 = module.custom_message.arn
    #define_auth_challenge          = "arn:aws:lambda:us-east-1:123456789012:function:define_auth_challenge"
    #post_authentication            = "arn:aws:lambda:us-east-1:123456789012:function:post_authentication"
    post_confirmation              = module.post_confirmation.arn
    #pre_authentication             = "arn:aws:lambda:us-east-1:123456789012:function:pre_authentication"
    pre_sign_up                    = module.pre_sign_up.arn
    #pre_token_generation           = "arn:aws:lambda:us-east-1:123456789012:function:pre_token_generation"
    #user_migration                 = "arn:aws:lambda:us-east-1:123456789012:function:user_migration"
    #verify_auth_challenge_response = "arn:aws:lambda:us-east-1:123456789012:function:verify_auth_challenge_response"
  }

  tags = var.tags
}

resource "aws_iam_role" "iam_for_lambda_cognito" {
  name               = "iam_for_lambda_cognito_${var.env}"
  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "lambda.amazonaws.com"
      },
      "Effect": "Allow",
      "Sid": ""
    }
  ]
}
EOF
}

/* custom_message */
module "custom_message" {
  source  = "../lambda"
  name    = "cognito_custom_message2"
  role    = aws_iam_role.iam_for_lambda_cognito.arn
  handler = "lambda_function.lambda_handler"
  runtime = "python3.8"
  tags    = var.tags
}

/* post_confirmation */
module "post_confirmation" {
  source  = "../lambda"
  name    = "cognito_post_confirmation"
  role    = aws_iam_role.iam_for_lambda_cognito.arn
  handler = "lambda_function.lambda_handler"
  runtime = "python3.8"
  tags    = var.tags
}

/* pre_sign_up */
module "pre_sign_up" {
  source  = "../lambda"
  name    = "cognito_pre_sign_up"
  role    = aws_iam_role.iam_for_lambda_cognito.arn
  handler = "lambda_function.lambda_handler"
  runtime = "python3.8"
  tags    = var.tags
}
