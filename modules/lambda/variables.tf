variable "name" {
  type        = string
  default     = "my_lambda"
}

variable "runtime" {
  type        = string
  default     = "python3.8"
}

variable "handler" {
  type        = string
  default     = "lambda_function.lambda_handler"
}

variable "timeout" {
  type        = number
  default     = 10
}

variable "role" {
  type        = string
  default     = "arn"
}

variable "tags" {
  type        = map(any)
  description = "aws tag commun"
  default = {
    Name = "assurly-back"
  }
}