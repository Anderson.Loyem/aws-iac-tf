#Grab the latest terraform image
FROM python:3.8

# Install 
RUN apt-get update -y && apt-get install apt-utils -y && apt-get upgrade -y && apt-get install tree

RUN apt-get install unzip -y
RUN apt-get install wget -y

################################
# Install Terraform
################################
# Download terraform for linux
RUN wget https://releases.hashicorp.com/terraform/1.0.0/terraform_1.0.0_linux_amd64.zip
RUN unzip terraform_1.0.0_linux_amd64.zip
RUN mv terraform /usr/local/bin/
RUN terraform --version 

################################
# End Install Terraform
################################

RUN apt-get install -y awscli

# Install dependencies
ADD ./requirements.txt /tmp/requirements.txt
RUN pip install -r /tmp/requirements.txt
# Add our code
ADD ./ /
WORKDIR /
