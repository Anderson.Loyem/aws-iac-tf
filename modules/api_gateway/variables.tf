variable "aws_api_gateway_rest_api_id" {
  type        = string
  default     = "my_lambda"
}

variable "aws_api_gateway_resource_id" {
  type        = string
  default     = "python3.8"
}

variable "integration_http_method" {
  type        = string
  default     = ""
}

variable "lambda_invoke_arn" {
  type        = string
  default     = ""
}

variable "lambda_name" {
  type        = string
  default     = "arn"
}

variable "tags" {
  type        = map(any)
  description = "aws tag commun"
  default = {
    Name = "assurly-back"
  }
}