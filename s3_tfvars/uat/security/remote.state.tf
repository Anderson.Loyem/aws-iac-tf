terraform {
  backend "s3" {
    bucket = "terraform-aws-assurly"
    key    = "security.uat.tfstate"
    region = "eu-west-3"
  }
}